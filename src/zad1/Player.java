package zad1;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Player extends Hand 
{	
	int bet;
	
	public Player(BlackJack current) 
	{
		value = 0;
		game = current;
		cards = new ArrayList<Integer>();
		
		Random randGen = new Random();
		this.addCard(game.deck.remove(randGen.nextInt(game.deck.size())));
		this.addCard(game.deck.remove(randGen.nextInt(game.deck.size())));
	}
	
	@Override
	public void print()
	{
		System.out.print("Player: ");
		for(int card: cards)
			if(card == 11)
				System.out.print("J ");
			else if(card == 12)
				System.out.print("Q ");
			else if(card == 13)
				System.out.print("K ");
			else if(card == 14 || card == 1)
				System.out.print("A ");
			else
				System.out.print(card + " ");
		System.out.println("Value = " + value);
	}
	
	public boolean play()
	{
		int decision = getDecision();
		
		Random randGen = new Random();
				
		while(decision != 0)
		{
			if(decision == 2)
			{
				this.addCard(game.deck.remove(randGen.nextInt(game.deck.size())));
				bet = 2 * bet;
				break;
			}
			this.addCard(game.deck.remove(randGen.nextInt(game.deck.size())));
			
			if(value > 21)
			{
				print();
				break;
			}
			
			decision = getDecision();
		}
		
		if(value > 21)
			return false;
		return true;
	}
	
	int getDecision()
	{
		print();
		System.out.println("Choose an option:");
		System.out.println("	0 - stand");
		System.out.println("	1 - hit");
		System.out.println("	2 - double");
		
		Scanner scanner = new Scanner(System.in);
		return Integer.parseInt(scanner.nextLine());
	}
}
