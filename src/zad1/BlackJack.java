package zad1;

import java.util.ArrayList;
import java.util.Scanner;

public class BlackJack
{
	ArrayList<Integer> deck;
	
	public BlackJack()
	{
		Scanner scanner = new Scanner(System.in);
		int bet = 0;
		int playerMoney = 100;
		do
		{
			deck = new ArrayList<Integer>();
			for(int i = 2; i < 15; ++i)
				for(int j = 0; j < 4; ++j)
					deck.add(i);

			Player player = new Player(this);
			Dealer dealer = new Dealer(this);

			player.print();
			dealer.print();
			System.out.println();
			
			do
			{
				System.out.println("Your money = " + playerMoney);
				System.out.print("Bet: ");
				bet = Integer.parseInt(scanner.nextLine());
				if(bet > playerMoney || bet <= 0)
					System.out.println("Wrong value!");
			} while(bet > playerMoney || bet <= 0);
			player.bet = bet;
			System.out.println();

			if(player.play() == false)
			{
				playerMoney -= player.bet;
				System.out.println("Dealer wins!");
			}
			else if(dealer.play() == false)
			{
				playerMoney += player.bet;
				System.out.println("Player wins!");
			}
			else if(dealer.value < player.value)
			{
				playerMoney += player.bet;
				System.out.println("Player wins!");
			}
			else
			{
				playerMoney -= player.bet;
				System.out.println("Dealer wins!");
			}
			System.out.println("---------------------------------");
		} while(playerMoney > 0);
		System.out.println("You lost all your money! Game over.");
	}
}
