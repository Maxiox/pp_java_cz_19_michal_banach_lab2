package zad1;

import java.util.ArrayList;

abstract public class Hand
{
	ArrayList<Integer> cards;
	public int value;
	BlackJack game;
	
	public Hand()
	{
		value = 0;
		cards = new ArrayList<Integer>();
	}
	
	public void addCard(int card)
	{
		cards.add(card);
		computeValue();
	}
	
	void computeValue()
	{
		value = 0;
		for(int card: cards)
			if(card <= 10)
				value += card;
			else if(card == 14)
				value += 11;
			else
				value += 10;
		while(value > 21 && cards.contains(14))
		{
			cards.set(cards.indexOf(14), 1);
			computeValue();
		}
	}
	
	abstract void print();
	abstract boolean play(); // false = hand.value > 21
	
}
