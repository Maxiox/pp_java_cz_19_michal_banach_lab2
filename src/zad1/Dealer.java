package zad1;

import java.util.ArrayList;
import java.util.Random;

public class Dealer extends Hand
{
	boolean show;

	public Dealer(BlackJack current)
	{
		show = false;
		game = current;
		value = 0;
		cards = new ArrayList<Integer>();
		
		Random randGen = new Random();
		this.addCard(game.deck.remove(randGen.nextInt(game.deck.size())));
		this.addCard(game.deck.remove(randGen.nextInt(game.deck.size())));
	}

	@Override
	public void print() 
	{
		System.out.print("Dealer: ");
		for(int i = 0; i < cards.size(); ++i)
			if(show == false && i == 0)
				System.out.print("X ");
			else if(cards.get(i) == 11)
				System.out.print("J ");
			else if(cards.get(i) == 12)
				System.out.print("Q ");
			else if(cards.get(i) == 13)
				System.out.print("K ");
			else if(cards.get(i) == 14 || cards.get(i) == 1)
				System.out.print("A ");
			else
				System.out.print(cards.get(i) + " ");	
		if(show == true)
			System.out.println("Value = " + value);
		else
			System.out.println();
	}
	
	public boolean play()
	{
		show = true;
		print();
		int decision = value >= 16 ? 0 : 1;
		
		Random randGen = new Random();
				
		while(decision != 0)
		{
			if(decision == 2)
			{
				this.addCard(game.deck.remove(randGen.nextInt(game.deck.size())));
				break;
			}
			this.addCard(game.deck.remove(randGen.nextInt(game.deck.size())));
			
			print();
			if(value > 21)
				break;
			
			decision = value >= 16 ? 0 : 1;
		}
		
		if(value > 21)
			return false;
		return true;
	}

}
